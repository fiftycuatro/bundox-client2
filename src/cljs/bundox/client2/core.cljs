(ns bundox.client2.core
  (:require-macros [cljs.core.async.macros :refer [go]])
  (:require
    [reagent.core :as reagent]
    [cljs-http.client :as http]
    [cljs.core.async :refer [<!]]))

(defonce state
  (reagent/atom
    {:documents []}))

(def documents (reagent/cursor state [:documents]))
(def selected-doc (reagent/cursor state [:selected-doc]))
(def search-results (reagent/cursor state [:search-results]))
(def selected-result (reagent/cursor state [:selected-result]))
(def search-term (reagent/cursor state [:search-term]))

(defn clear-search-results!
  []
  (swap! state dissoc :search-term :search-results :selected-result))

(defn header
  []
  [:header {:style {:margin-top "5px"}}
   [:img {:style {:margin-left "5px"
                  :height "35px"}
          :src "/img/bundox_logo_nav.png"}]])

(defn footer
  []
  [:footer
   {:style {:display :flex
            :justify-content :center}}
   [:p.thin "Copyright (c) 2018 Phillip Gomez"]])

(def bundox-root-url "http://localhost:8080")
(def bundox-content-type "application/bundox.api.v1+json")
(def bundox-documents-url (str bundox-root-url "/bundox/api/documents/"))
(def search-base-url (str bundox-root-url "/bundox/api/documents/documentation"))

(defn json->cljs
  [json]
  (js->clj (.parse js/JSON json) :keywordize-keys true))

(def request-options
  {:headers {"Content-Type" bundox-content-type}
   :with-credentials? false})

(defn search!
  [term]
  (go (let [res (<! (http/get search-base-url
                              (assoc request-options
                                :query-params {"maxResults" 50 "searchTerm" term})))
            results (json->cljs (:body res))]
        (reset! search-results results))))

(defn all-documents!
  []
  (go (let [res (<! (http/get bundox-documents-url request-options))
            results (json->cljs (:body res))]
        (reset! documents results))))

(defn search-term-input
  []
  [:input.input.is-small
   {:type "text"
    :value @search-term
    :on-change #(let [val (-> % .-target .-value)]
                  (if (empty? val)
                    (clear-search-results!)
                    (reset! search-term val)))}])

(defn search-btn
  []
  [:a.button.is-primary.is-small
   {:on-click #(search! @search-term)}
   [:i.fas.fa-search]])

(defn close-search-btn
  []
  [:a.button.is-small
   {:on-click #(clear-search-results!)}
   [:i.fas.fa-times-circle]])

(defn search-bar-ui
  []
  [:div.field.is-grouped {:style {:margin "2px"}}
   [search-term-input]
   (if @search-results
     [close-search-btn]
     [search-btn])])

(defn result-list-item-ui
  [{:keys [documentId subject] :as result}]
  ^{:key (str documentId "-" subject)}
  [:li
   {:on-click #(reset! selected-result result)}
   subject])

(defn results-list-ui
  []
  (fn [results]
    [:ul
     (doall (map result-list-item-ui results))]))

(defn doc-list-item-ui
  [{:keys [id name version] :as doc}]
  ^{:key id} [:li
              {:on-click #(reset! selected-doc doc)}
              (str name " - " version)])

(defn documents-list-ui
  []
  (fn [docs]
    [:ul
     (doall (map doc-list-item-ui docs))]))

(defn left-pane
  []
  [:div
   [search-bar-ui]
   (if @search-results
     [results-list-ui @search-results]
     [documents-list-ui @documents])])

(defn right-pane 
  []
  [:p "Right"])

(defn empty-browser-ui
  []
  [:div " <- Pick a Document"])

(defn doc-browser-ui
  []
  (let [selected (or (:path @selected-result)
                     (:indexPath @selected-doc))]
    (if selected
      [:iframe {:style {:height "100%"
                        :width "100%"}
                :src (str bundox-root-url selected)}]
      [empty-browser-ui])))

(defn content
  []
   [doc-browser-ui])

(defn page-layout
  [{:keys [header content left right footder]}]
  [:div.layout-container
   [:div.border-bottom [header]]
   [:div.layout-main
    [:div.layout-content [content]]
    (when left
      [:div.layout-left.border-right [left]])
    (when right 
      [:div.layout-right.border-left [right]])]
   [:div.border-top [footer]]])
 
(defn page
  []
  [page-layout {:header header
                :content content
                :left left-pane
                :footer footer}])

(defn ^:export run []
  (all-documents!)
  (reagent/render [#'page]
                  (js/document.getElementById "app")))
